const https = require('https');
const express = require('express')
const serveStatic = require('serve-static')
const path = require('path')

const fs = require('fs');

const gatewayKey  = fs.readFileSync('gateway.key', 'utf8');
const gatewayCrt = fs.readFileSync('gateway.crt', 'utf8');

const credentials = { key: gatewayKey, cert: gatewayCrt };

const ui = express()

ui.use(serveStatic(path.join(__dirname, '.')))

const httpsServer = https.createServer(credentials, ui);
httpsServer.listen(3000, () => {
    console.log(`UI Gateway ready on port: 3000`);
});