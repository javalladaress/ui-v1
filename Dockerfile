FROM mhart/alpine-node:12
COPY . /ui/
WORKDIR /ui
RUN npm install
EXPOSE 3000/tcp
CMD ["npm", "run", "pro"]
