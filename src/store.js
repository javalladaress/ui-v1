import {
  createStore,
  compose as origCompose,
  applyMiddleware,
  combineReducers
} from 'redux';
import thunk from 'redux-thunk';
import { lazyReducerEnhancer } from 'pwa-helpers/lazy-reducer-enhancer.js';

import id from './middleware/id';
import log from './middleware/log';
import api from './middleware/api';
import logout from './middleware/logout';

import gauth from './reducers/gauth.reducer';
import auth from './reducers/auth.reducer';
import ui from './reducers/ui.reducer';
import accounts from './reducers/accounts.reducer'
import banxico from './reducers/banxico.reducer'

const compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || origCompose;
const APP_STATE = 'APP_STATE';

const saveState = (state) => {
  const { auth, gauth } = state;
  localStorage.setItem(APP_STATE, JSON.stringify({ auth, gauth }));
}

const loadState = () => {
  let state = JSON.parse(localStorage.getItem(APP_STATE) || '{}');
  return state || undefined;
}

export const store = createStore(
  (state, action) => state, // eslint-disable-line no-unused-vars
  loadState(),
  compose(
    lazyReducerEnhancer(combineReducers), 
    applyMiddleware(thunk, id, log, api, logout)
  )
);

export {
  saveState,
  loadState
}

store.addReducers({ gauth, auth, ui, accounts, banxico });

const unsubscribe = store.subscribe(() => {
  saveState(store.getState());
});