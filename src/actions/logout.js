import * as actions from '../consts/action-types';

const onLogoutSuccess = (payload) => ({
    type: actions.LOGOUT, payload
});

const logout = (auth) => ({
	type: actions.API,
    meta: {
        url: `/o/logout/${auth.id}/${auth.sid}`,
        method: "GET",
        onSuccess: onLogoutSuccess
    }
});

export default logout;