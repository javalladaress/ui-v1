import * as actions from '../consts/action-types';

const setRouter = (router) => ({
    type: actions.SET_ROUTER, 
    payload: { 'router': router }
});

const setLoading = (isLoading) => ({
    type: actions.SET_LOADING, 
    payload: {
        loading: isLoading
    }
});

const ui = Object.freeze({
    setRouter,
    setLoading
})
  
export default ui
export { 
    setRouter,
    setLoading
}