import * as actions from '../consts/action-types';

const setAccounts = (data) => ({
    type: actions.SET_ACCOUNTS, payload: data.accounts
});

const setAccount = (data) => ({
    type: actions.SET_ACCOUNT, 
    payload: data.account
});

const setAccountActivity = (data) => ({
    type: actions.SET_ACCOUNT_ACTIVITY, 
    payload: data.activity
});

const setAccountActivityTotal = (data) => ({
    type: actions.SET_ACCOUNT_ACTIVITY_TOTAL, 
    payload: data.data
});

const setLastAccountActivity = (data) => ({
    type: actions.SET_LAST_ACCOUNT_ACTIVITY, 
    payload: data.activity
});

const unsetAccount = () => ({
	type: actions.UNSET_ACCOUNT
});

const unsetAccounts = () => ({
	type: actions.UNSET_ACCOUNTS
});

const getAccounts = () => ({
	type: actions.API,
    meta: {
        url: "/accounts/",
        method: "get",
        onSuccess: setAccounts
    }
});

const getAccount = (accountId) => ({
	type: actions.API,
    meta: {
        url: `/accounts/${accountId}`,
        method: "get",
        onSuccess: setAccount
    }
});

const getAccountActivity = (accountId, first, size) => ({
	type: actions.API,
    meta: {
        url: `/accounts/${accountId}/activity?first=${first||0}&size=${size||1}`,
        method: "get",
        onSuccess: setAccountActivity
    }
});

const getAccountActivityTotal = (accountId) => ({
	type: actions.API,
    meta: {
        url: `/accounts/${accountId}/activity/total`,
        method: "get",
        onSuccess: setAccountActivityTotal
    }
});

const updateAccount = (accountId, acct) => ({
    type: actions.API,
    payload: {
        account: {
            accountId: acct.id,
            description: acct.desc
        }
    },
    meta: {
        url: `/accounts/${accountId}`,
        method: "put",
        onSuccess: setAccountActivity
    }
});

const createAccountActivity = (accountId, amount) => ({
    type: actions.API,
    payload: {
        activity: {
            'id': accountId,
            'amount': amount
        }
    },
    meta: {
        url: `/accounts/${accountId}/activity?amount=${amount}`,
        method: "post",
        onSuccess: setLastAccountActivity
    }
});

const accounts = Object.freeze({
    unsetAccount,
    unsetAccounts,
    getAccounts,
    getAccount,
    updateAccount,
    getAccountActivityTotal,
    createAccountActivity
})
  
export default accounts
export { 
    unsetAccount,
    unsetAccounts,
    getAccounts,
    getAccount,
    updateAccount,
    getAccountActivity,
    getAccountActivityTotal,
    createAccountActivity
}