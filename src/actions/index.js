import login from './login'
import logout from './logout'
import setGAuth from './glogin'
import {
    setRouter,
    setLoading
} from './ui.actions'
import {
    unsetAccount,
    unsetAccounts,
    getAccounts,
    getAccount,
    updateAccount,
    getAccountActivity,
    getAccountActivityTotal,
    createAccountActivity
} from './accounts.actions'
import {
    getExhangeRates
} from './banxico.actions'

const actions = Object.freeze({
    setRouter,
    setLoading,
    setGAuth,
    login, 
    logout,
    unsetAccount,
    unsetAccounts,
    getAccounts,
    getAccount,
    updateAccount,
    getExhangeRates,
    getAccountActivity,
    getAccountActivityTotal,
    createAccountActivity
})
  
export default actions
export { 
    setRouter,
    setLoading,
    setGAuth,
    login,
    logout,
    unsetAccount,
    unsetAccounts,
    getAccounts,
    getAccount,
    updateAccount,
    getExhangeRates,
    getAccountActivity,
    getAccountActivityTotal,
    createAccountActivity
}