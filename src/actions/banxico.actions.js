import * as actions from '../consts/action-types';

const setExchangeRates = (data) => ({
    type: actions.SET_EXCHANGE_RATES, payload: data.bmx
});

const getExhangeRates = (accountId) => ({
	type: actions.API,
    meta: {
        url: `/bmx/SieAPIRest/service/v1/series/SF43718,SF46410/datos/oportuno`,
        method: "get",
        onSuccess: setExchangeRates
    }
});

const accounts = Object.freeze({
    getExhangeRates
})
  
export default accounts
export { 
    getExhangeRates
}