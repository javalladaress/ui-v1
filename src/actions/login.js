import * as actions from '../consts/action-types';

const onLoginSuccess = (auth) => ({
    type: actions.SET_AUTH,
    payload: auth
});

const login = (auth) => ({
	type: actions.API,
	payload: {
        'email': auth.email,
        'token': auth.token
    },
    meta: {
        url: "/i/login",
        method: "post",
        onSuccess: onLoginSuccess
    }
});

export default login;