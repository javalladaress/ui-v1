
import * as actions from '../consts/action-types';

const setGAuth = (auth) => ({
	type: actions.SET_GAUTH,
	payload: auth
});

export default setGAuth;