import { SET_AUTH, UNSET_AUTH } from '../consts/action-types';

const authReducer = (auth = {}, action) => {

	const { payload } = action;

	switch(action.type) {
		case SET_AUTH:
			return payload;
	    case UNSET_AUTH:
			return {};
		default:
			return auth;
	}
	
};

export default authReducer