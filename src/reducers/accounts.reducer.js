import { createReducer } from '@reduxjs/toolkit'
import { createSelector } from 'reselect'
import { nanoid } from 'nanoid';
import { 
	SET_ACCOUNT, 
	SET_ACCOUNTS,
	SET_ACCOUNT_ACTIVITY,
	SET_ACCOUNT_ACTIVITY_TOTAL,
	SET_LAST_ACCOUNT_ACTIVITY,
	UNSET_ACCOUNT,
	UNSET_ACCOUNTS
} from '../consts/action-types';
import * as R from 'ramda'
import moment from 'moment';

const { mapObjIndexed, merge } = R

const currencyFormat = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' });

const initialState = {
	allAccounts: [],
	selectedAccount: {},
	selectedAccountActivity: [],
	selectedAccountActivityTotal: {},
	lastAccountActivity: {}
}

const getPayload = action => action.payload;
const toAccount = account => ({ id: account.accountId, desc: account.description });
const formatAmout = amount => (Math.round(amount * 100) / 100).toFixed(2)
const toAccountActivity = accountActivity => {
	const _amount = accountActivity.amount;
	const _rates = accountActivity.rates;
	const _timestamp = accountActivity.timestamp;
	const _amounts = merge({ 'MXP': formatAmout(_amount) }, mapObjIndexed((value, key) => formatAmout(_amount / value), _rates));
	return {
		id: accountActivity.account, rates: _rates, timestamp: _timestamp, dateStr: moment(_timestamp).format("DD/MM/YYYY hh:mm:ss"),
		amounts: _amounts, amountsStr: mapObjIndexed((value, key) => currencyFormat.format(value), _amounts), uuid: nanoid()
	};
};
const toAccountActivityTotal = accountActivityTotal => {
	return { 
		account: accountActivityTotal._id,
		total: accountActivityTotal.total,
		totalStr: currencyFormat.format(accountActivityTotal.total),
		count: accountActivityTotal.count
	};
};

const selectAllAccounts = createSelector([getPayload], accounts => accounts.map(toAccount));
const selectAccount = createSelector([getPayload], toAccount);
const selectAccountActivity = createSelector([getPayload], activity => activity.map(toAccountActivity));
const selectLastAccountActivity = createSelector([getPayload], toAccountActivity);
const selectAccountActivityTotal = createSelector([getPayload], toAccountActivityTotal);

const accounts = createReducer(initialState, {
	SET_ACCOUNTS: (state, action) => {
		state.allAccounts = selectAllAccounts(action)
	},
	SET_ACCOUNT: (state, action) => {
		state.selectedAccount = selectAccount(action)
	},
	SET_ACCOUNT_ACTIVITY: (state, action) => {
		state.selectedAccountActivity = selectAccountActivity(action)
	},
	SET_ACCOUNT_ACTIVITY_TOTAL: (state, action) => {
		state.selectedAccountActivityTotal = selectAccountActivityTotal(action)
	},
	SET_LAST_ACCOUNT_ACTIVITY: (state, action) => {
		state.lastAccountActivity = selectLastAccountActivity(action)
	},
	UNSET_ACCOUNTS: (state, action) => {
		state.allAccounts = []
	},
	UNSET_ACCOUNT: (state, action) => {
		state.selectedAccount = {}
		state.selectedAccountActivity = []
		state.selectedAccountActivityTotal = {},
		state.lastAccountActivity = {}
	}
})

export default accounts