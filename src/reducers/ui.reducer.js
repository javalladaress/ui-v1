import { createReducer } from '@reduxjs/toolkit'
import { 
	SET_ROUTER,
	SET_LOADING
} from '../consts/action-types';

const initialState = {
	loading: false
}

const ui = createReducer(initialState, {
	SET_ROUTER: (state, action) => {
		state.router = action.payload.router;
	},
	SET_LOADING: (state, action) => {
		state.loading = action.payload.loading;
	}
})

export default ui