import { createReducer } from '@reduxjs/toolkit'
import { createSelector } from 'reselect'
import { 
	SET_EXCHANGE_RATES
} from '../consts/action-types';
import * as R from 'ramda'
const { forEach } = R

const initialState = {
	rates: {}
}

const serieToISO = serie => {

	switch( serie ) {

		case 'SF43718':
			return "USD";

		case 'SF46410':
			return "EUR"

		default: /* NO_GO */

	}

	return null;
};

const getSeries = action => toExhangeRates(action.payload.series)
const toExhangeRate = serie => { 
	const _id = serie.idSerie;
	return { 
		bmx: _id,
		iso: serieToISO(_id) || "XXX",
		rate: serie.datos[0].dato
	};
};
const toExhangeRates = series => series.map(toExhangeRate)
const toExhangeRatesObject = (rates) => {
	const _rates = {};
	forEach(rate => { _rates[rate.iso] = rate.rate }, rates);
	return _rates;
}

const selectExchangeRates = createSelector([getSeries], toExhangeRatesObject);

const banxico = createReducer(initialState, {
	SET_EXCHANGE_RATES: (state, action) => {
		state.rates = selectExchangeRates(action)
	}
})

export default banxico