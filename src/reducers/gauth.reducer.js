import { SET_GAUTH, UNSET_GAUTH } from '../consts/action-types';

const gauthReducer = (gauth = {}, action) => {

	const { payload } = action;

	switch(action.type) {
		case SET_GAUTH:
			return payload;
	    case UNSET_GAUTH:
			return {};
		default:
			return gauth;
	}
	
};

export default gauthReducer