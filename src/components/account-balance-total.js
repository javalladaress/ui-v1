import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';

import * as R from 'ramda'
const { isNil } = R

class AccountBalanceTotal extends connect(store)(LitElement) {

	static get properties() {
		return {
			totals: { type: Object }
		};
	}

	constructor() {
		super();
		this.totals = {};
	}

	firstUpdated(changedProperties) { 
		Metro.initWidgets([]);
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		const { total, count, totalStr } = this.totals
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div id="box" class="more-info-box fg-white bg-darkCyan mt-4 drop-shadow">
				<div class="content ml-16">
					<h2 class="text-bold mb-0">${totalStr || '?.??'}</h2>
					<div>${count || '?'} movimientos</div>
				</div>
				<div class="icon mr-16">
					${ (total >= 0)?
						html`<span class="mif-arrow-up fg-white"></span>`:
						html`<span class="mif-arrow-down fg-white"></span>`
					 }
				</div>
			</div>
			${this.script()}
		`;
	}

	updated(changedProperties) {
		const box = this.shadowRoot.getElementById('box');
		
		const total = this.totals.total;

		if( isNil(total) ) {
			box.classList.add('bg-darkCyan');
			return;
		}

		box.classList.remove('bg-darkCyan');

		if( total >= 0 ) {
			box.classList.remove('bg-red');
			box.classList.add('bg-green');
		} else {
			box.classList.remove('bg-green');
			box.classList.add('bg-red');
		}

	}

	stateChanged(state){

	}

}

customElements.define('account-balance-total', AccountBalanceTotal);
