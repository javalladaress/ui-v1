import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import * as R from 'ramda'

const { merge } = R

class UpdateAccountDialog extends connect(store)(LitElement) {

	static get properties() {
		return {
			account: { type: Object },
			opened: { type: Boolean },
			updating: { type: Boolean },
			amount: { type: Number },
			operation: { type: String }
		};
	}

	constructor() {
		super();
		this.opened = false;
		this.updating = false;
		this.amount = 0.0;
		this.operation = "...";
	}

	firstUpdated(changedProperties) { 
		Metro.initWidgets([
			this.shadowRoot.getElementById("createMovementDialog"),
			this.shadowRoot.getElementById("amount")
		]);
	}

	disconnectedCallback() {
		const dialog = document.getElementById("createMovementDialog");
		dialog.parentNode.removeChild(dialog);
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		const { id } = this.account;
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div id="createMovementDialog" class="dialog" data-role="dialog">
				<div id="createMovementDialogHeader" class="dialog-title fg-white bg-darkCyan text-bold"><b>${id}</b></div>
				<div class="dialog-content">
					<input id="amount" type="text" data-role="input" type="number"
							data-clear-button="false" .value="${this.amount}"
							@input="${(e) => this.setAmount(e)}" ?disabled="${this.updating}">
				</div>
				<div class="dialog-actions">
					<button class="button secondary" @click="${this.cancelDialog}" ?disabled="${this.updating}">Cancelar</button>
					<button id="createMovementDialogOperation" class="button primary" @click="${this.updateAmount}" ?disabled="${this.updating}" .value="${this.operation}">${this.operation}</button>
				</div>
			</div>
			${this.script()}
		`;
	}

	updateAmount() {
		

		if( isNaN(this.amount) || !this.amount || (this.amount == 0) ) { return; }

		this.updating = true;

		this.dispatchEvent(new CustomEvent('dialog-confirm', { 
			detail: merge(this.account, { amount: this.amount })
		}));
	}

	setAmount(e) {
		const amount = e.target.value;
		
		const header = document.getElementById("createMovementDialogHeader");
		const operation = document.getElementById("createMovementDialogOperation");

		if( isNaN(amount) || !amount || (amount == 0) ) {
			header.classList.remove("bg-red", "bg-green");
			header.classList.add("bg-darkCyan");
			operation.classList.add("primary");
			operation.classList.remove("success", "alert");
			this.operation = "...";
			return;
		}

		header.classList.remove("bg-darkCyan");

		if( amount > 0 ) {
			header.classList.remove("bg-red");
			header.classList.add("bg-green");
			operation.classList.add("success");
			operation.classList.remove("alert");
			this.operation = "ABONAR";
		} else {
			header.classList.remove("bg-green");
			header.classList.add("bg-red");
			operation.classList.add("alert");
			operation.classList.remove("success");
			this.operation = "CARGAR";
		}

		this.amount = amount;
	}

	cancelDialog() {
		this.amount = 0.00;
		this.dispatchEvent(new CustomEvent('dialog-cancel'));
	}

	updated(changedProperties) {

		if( changedProperties.has('opened') ) {

			if( this.opened ) {
				Metro.dialog.open('#createMovementDialog');
			} else {
				const header = document.getElementById("createMovementDialogHeader");
				const operation = document.getElementById("createMovementDialogOperation");

				header.classList.remove("bg-red", "bg-green");
				header.classList.add("bg-darkCyan");
				operation.classList.add("primary");
				operation.classList.remove("success", "alert");
				this.operation = "...";

				Metro.dialog.close('#createMovementDialog');
				this.updating = false;
				this.amount = 0.00;
			}

		}

		if( changedProperties.has('account') ) {
			this.amount = 0.00;
		}
	}

	stateChanged(state){ }

}

customElements.define('update-account-dialog', UpdateAccountDialog);
