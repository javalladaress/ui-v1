import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';

class AccountBalanceHistory extends connect(store)(LitElement) {

	static get properties() {
		return {
			account: { type: Object },
			activity: { type: Array },
			rates: { type: Object }
		};
	}

	constructor() {
		super();
		this.account = {};
		this.activity = [];
		this.rates = {};
	}

	firstUpdated(changedProperties) {
		Metro.initWidgets([]);
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div class="skill-box drop-shadow rounded">
				<div class="header bg-lightAmber fg-white drop-shadow">
					<div class="title text-bold">MOVIMIENTOS</div>
					<div class="subtitle"></div>
				</div>
				<ul class="skills">
					${this.activity.map(item => html`
						<li>
							<span>
								${(item.amounts.MXP > 0)?
								html`<button class="button success cycle mini" title="abono"></button>`:
								html`<button class="button alert cycle mini" title="cargo"></button>`}
								<span class="text-bold fg-cyan ml-4">${item.dateStr}</span>
							</span>
							<span class="badge bg-darkCobalt fg-white rounded text-bold py-1 px-2 mx-1" 
									title="${item.amountsStr.EUR} @${item.rates.EUR} ( ${this.rates.EUR || '...'} )">
								${item.amountsStr.EUR} @${item.rates.EUR} ( ${this.rates.EUR || '...'} )
							</span>
							<span class="badge bg-crimson fg-white rounded text-bold py-1 px-2 mx-1" 
									title="${item.amountsStr.USD} @${item.rates.USD} ( ${this.rates.USD || '...'} )">
								${item.amountsStr.USD} @${item.rates.USD} ( ${this.rates.USD || '...'} )
							</span>
							<span class="badge bg-emerald fg-white rounded text-bold py-1 px-2 mx-1"
									title="${item.amountsStr.MXP}">${item.amountsStr.MXP}</span>
						</li>
					`)}
				</ul>
			</div>
			${this.script()}
		`;
	}

	updated(changedProperties) {
	}

	stateChanged(state){
	}

}

customElements.define('account-balance-history', AccountBalanceHistory);
