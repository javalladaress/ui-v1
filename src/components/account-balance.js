import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';

class AccountBalance extends connect(store)(LitElement) {

	static get properties() {
		return {
			account: { type: Object },
			activity: { type: Object },
			rates: { type: Object }
		};
	}

	constructor() {
		super();
		this.account = {};
		this.activity = {};
		this.rates = {};
	}

	firstUpdated(changedProperties) { 
		Metro.initWidgets([]);
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		const amounts = (this.activity.amounts || {});
		const amountsStr = (this.activity.amountsStr || {});
		const _rates = (this.activity.rates || {});
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div class="social-box text-center drop-shadow rounded">
				<div class="header bg-cyan fg-white drop-shadow">
					<div class="title pt-4">
						<span class="text-bold">${this.account.id}</span>
					</div>
					<div class="subtitle">${this.account.desc}</div>
				</div>
				<ul class="skills py-2">
					<li>
						<div class="text-bold  mb-2">${amountsStr.MXP || '...'}</div>
						<div>
							<span class="bg-emerald fg-white rounded text-bold border border-radius bd-red border-size-3 py-1 px-2">MXP</span>
						</div>
					</li>
					<li>
						<div class="text-bold  mb-2" title="@${_rates.USD || '...'} ( ${this.rates.USD || '...'} )">${amountsStr.USD || '...'}</div>
						<div>
							<span class="bg-crimson fg-white rounded text-bold border border-radius bd-darkCobalt border-size-3 py-1 px-2">USD</span>
						</div>
					</li>
					<li>
						<div class="text-bold mb-2" title="@${_rates.EUR || '...'} ( ${this.rates.EUR || '...'} )">${amountsStr.EUR || '...'}</div>
						<div>
							<span class="bg-darkCobalt fg-white rounded text-bold border border-radius bd-amber border-size-3 py-1 px-2">EUR</span>
						</div>
					</li>
				</ul>
			</div>
			${this.script()}
		`;
	}

	updated(changedProperties) {
	}

	stateChanged(state){
	}

}

customElements.define('account-balance', AccountBalance);
