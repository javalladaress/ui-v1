import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { Router } from '@vaadin/router';
import { store } from '../store.js';
import { 
	getAccount,
	updateAccount,
	getExhangeRates,
	getAccountActivity,
	getAccountActivityTotal,
	createAccountActivity
} from '../actions';

import * as R from 'ramda'
import moment from 'moment';

import './account-balance.js';
import './account-balance-history.js';
import './account-balance-chart.js';
import './account-balance-total.js';
import './edit-account-dialog.js';
import './update-account-dialog.js';

const { equals, isEmpty, groupBy, mapObjIndexed, reduce, forEach, keys, values, isNil } = R

class AccountDetails extends connect(store)(LitElement) {

	static get properties() {
		return {
			account: { type: Object },
			accountActivity: { type: Array },
			accountActivityTotal: { type: Object },
			exchangeRates: { type: Object },
			openEditAccoutDialog: { type: Boolean },
			createMovementDialogOpened: { type: Boolean },
			lastAccountActivity: { type: Object },
			datasets: { type: Object }
		};
	}

	onBeforeEnter(location, commands) {
		const accountId = location.params.id;
		store.dispatch(getAccount(accountId));
		store.dispatch(getExhangeRates());
		store.dispatch(getAccountActivity(accountId, 0, 10));
		store.dispatch(getAccountActivityTotal(accountId));
	}

	onBeforeLeave(location, commands) {
		this.reset();
	}

	constructor() {
		super();
		this.reset();
	}

	reset() {
		this.account = {};
		this.accountActivity = [];
		this.accountActivityTotal = {}
		this.exchangeRates = {}
		this.openEditAccoutDialog = false;
		this.createMovementDialogOpened = false;
		this.datasets = {}
	}

	firstUpdated(changedProperties) {
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	actionsTemplate() {
		return html`
		<div>
			<button class="tool-button secondary small outline text-button" @click="${(e) => Router.go('/')}">REGRESAR</button>
			<button class="tool-button primary small outline text-button" @click="${this.openEditAccountDialog}">EDITAR</button>
			<button class="tool-button warning small outline text-button" @click="${this.openCreateMovementDialog}">CARGAR / ABONAR</button>
		</div>
		`
	}

	render() {
		const { account } = this;
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			${this.actionsTemplate()}
			<account-balance .account="${account}" .activity="${this.accountActivity[0] || {}}" .rates="${this.exchangeRates}"></account-balance>
			<account-balance-total .totals="${this.accountActivityTotal}"></account-balance-total>
			<account-balance-history .account="${account}" .activity="${this.accountActivity}" .rates="${this.exchangeRates}"></account-balance-history>
			<div class="mt-4"><account-balance-chart .datasets="${this.datasets}"></account-balance-chart></div>
			<edit-account-dialog .account="${account}" .opened=${this.openEditAccoutDialog} 
									@dialog-cancel="${this.closeEditAccountDialog}"
									@dialog-confirm="${this.updateAccount}"></edit-account-dialog>
			<update-account-dialog .account="${account}" .opened=${this.createMovementDialogOpened}
									@dialog-cancel="${this.closeCreateMovementDialog}"
									@dialog-confirm="${this.processAccountActivity}"></update-account-dialog>
			${this.script()}
		`;
	}

	openEditAccountDialog() {
		this.openEditAccoutDialog = true;
	}

	closeEditAccountDialog() {
		this.openEditAccoutDialog = false;
	}

	openCreateMovementDialog() {
		this.createMovementDialogOpened = true;
	}

	closeCreateMovementDialog() {
		this.createMovementDialogOpened = false;
	}

	updateAccount(e) {
		const { id } = this.account;
		const account = e.detail;

		if( id === account.id ) {
			store.dispatch(updateAccount(id, account));
		}
	}

	processAccountActivity(e) {
		const { id } = this.account;
		const movement = e.detail;

		console.log(id, (Math.round(movement.amount * 100) / 100).toFixed(2));

		store.dispatch(createAccountActivity(id, (Math.round(movement.amount * 100) / 100).toFixed(2)));
	}

	setAccount(account) {
		this.account = account
	}

	setAccountActivity(accountActivity) {
		this.accountActivity = accountActivity

		const groupesByDate = groupBy(function (activity) {
			console.log(activity);
			return moment(activity.timestamp).startOf('day').format("DD/MM/YYYY")
		})(accountActivity);

		const totalsByDay = mapObjIndexed(function(activities, day) {
			return reduce((accum, value) => (accum + parseFloat(value.amounts.MXP)), 0, activities);
		}, groupesByDate);

		const negativeTotalsByDay = mapObjIndexed(function(activities, day) {
			var total = 0.0;
			forEach(function(activity) {
				const amount = -1.0 * parseFloat(activity.amounts.MXP);
				total = total + ((amount > 0)? amount : 0.0);
			}, activities)
			return total;
		}, groupesByDate);

		const positiveTotalsByDay = mapObjIndexed(function(activities, day) {
			var total = 0.0;
			forEach(function(activity) {
				const amount = parseFloat(activity.amounts.MXP);
				total = total + ((amount > 0)? amount : 0.0);
			}, activities)
			return total;
		}, groupesByDate);

		this.datasets = {
			labels: keys(totalsByDay), 
			totals: values(totalsByDay), 
			negatives: values(negativeTotalsByDay), 
			positives: values(positiveTotalsByDay)
		};

	}

	setAccountActivityTotal(accountActivityTotal) {
		this.accountActivityTotal = accountActivityTotal
	}

	setExchangeRates(exchangeRates) {
		this.exchangeRates = exchangeRates
	}

	setLastAccountActivity(lastAccountActivity) {
		this.lastAccountActivity = lastAccountActivity;
		let accountId = lastAccountActivity.id;
		store.dispatch(getAccountActivity(accountId, 0, 10));
		store.dispatch(getAccountActivityTotal(accountId));
	}

	stateChanged(state){
		const { ui, accounts, banxico } = state;
		const _selectedAccount = accounts.selectedAccount;
		const _selectedAccountActivity = accounts.selectedAccountActivity;
		const _selectedAccountActivityTotal = accounts.selectedAccountActivityTotal
		const _lastAccountActivity = accounts.lastAccountActivity

		const _exchangeRates = banxico.rates;

		!isEmpty(_selectedAccount) && !equals(_selectedAccount, this.account) && this.setAccount(_selectedAccount);
		!isEmpty(_selectedAccountActivity) && !equals(_selectedAccountActivity, this.accountActivity) && this.setAccountActivity(_selectedAccountActivity);
		!isEmpty(_exchangeRates) && !equals(_exchangeRates, this.exchangeRates) && this.setExchangeRates(_exchangeRates);
		!isEmpty(_selectedAccountActivityTotal) && !equals(_selectedAccountActivityTotal, this.accountActivityTotal) && this.setAccountActivityTotal(_selectedAccountActivityTotal);
		!isEmpty(_lastAccountActivity) && !equals(_lastAccountActivity, this.lastAccountActivity) && this.setLastAccountActivity(_lastAccountActivity);

		this.openEditAccoutDialog && (this.openEditAccoutDialog = false);
		this.createMovementDialogOpened && (this.createMovementDialogOpened = false);

		console.log(accounts);
		console.log(banxico);
	}

}

customElements.define('account-details', AccountDetails);
