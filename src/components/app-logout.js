import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { Router } from '@vaadin/router';
import { store } from '../store.js';
import { logout } from '../actions';

class AppLogout extends connect(store)(LitElement) {

	static get properties() {
		return {
			auth: { type: Object },
			gauth: { type: Object }
		};
	}

	constructor() {
		super();

		const { gauth, auth } = store.getState();

		this.auth = auth;
		this.gauth = gauth;
	}

	firstUpdated(changedProperties) {
		Metro.initWidgets([
			this.shadowRoot.getElementById("logout")
		]); 
		console.log(this.auth);
		store.dispatch(logout(this.auth));
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		const { gauth, auth } = this
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div id="logout" class="bg-darkCyan drop-shadow w-100 h-100" data-role="panel">
				<div class="social-box drop-shadow">
					<div class="header bg-orange fg-white">
						<img src="${gauth.image}" class="avatar">
						<div class="title text-bold">${gauth.name}</div>
						<div class="subtitle text-bold">${auth.id}</div>
					</div>
					<ul class="skills">
						<li>
							<button @click="${this.goLogin}" class="button success m-2 drop-shadow text-bold">Iniciar Sesión</button>
						</li>
					</ul>
				</div>
			</div>
			${this.script()}
		`;
	}

	goLogin() {		
		Router.go("/login");
	}

	updated(changedProperties) {
	}

	stateChanged(state){

	}

}

customElements.define('app-logout', AppLogout);
