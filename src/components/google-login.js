import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { Router } from '@vaadin/router';
import { store } from '../store.js';
import { setLoading, setGAuth, login } from '../actions';

class GoogleLogin extends connect(store)(LitElement) {
	
	static get properties() {
		return {
			loginReady: { type: Boolean },
			gauth: { type: Object },
			name: { type: String },
			email: { type: String },
			user: { type: String }
		};
	}

	onBeforeEnter(location, commands) {
		const { auth } = store.getState();

		if( auth.id ) {
			commands.redirect("/");
		}
	}

	onAfterEnter(location) { }

	onBeforeLeave(location, commands) { }

	constructor() {
		super();

		this.loginReady = false;

		this.initCallback = 'onGAPILoaded';

		this.user = "XXXXXXX";
		this.name = "Employee";
		this.email = "employee@bbva.com";

		this.gauth = null;
	}

	connectedCallback() {
		super.connectedCallback();

		if( !this.gauth ) {
			window[this.initCallback] = this._onGAPILoaded.bind(this);
			this._addGoogleJS();
		}

	}

	firstUpdated(changedProperties) { }

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render(){
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4/css/metro-all.min.css">
			<div class="container-fluid">
				<div class="card image-header w-50 drop-shadow">
					<div class="card-header img-container bg-darkCyan" style="background-image: url(http://lorempixel.com/1000/600/)">
						<div class="image-overlay op-dark"></div>
						<p class="fg-white text-bold"><b>BBVA</b> · Practitioner 4Ed</p>
					</div>
					<div class="card-content p-2 fg-darkCobalt">
						<div id="progress" data-role="progress" data-type="line" data-small="true"></div>
						<p>${this.name} ( <b>${this.user}</b> )</p>${this.email}
					</div>
					<div class="card-footer">
						<button class="image-button primary rounded large p-2" @click="${this.doLogin}" ?disabled="${!this.loginReady}">Google Login</button>
					</div>
				</div>
			</div>
			${this.script()}
		`;
	}

	_addGoogleJS() {
		var script = document.getElementById("gapi");
		
		if( script ) { 
			script.parentNode.removeChild(script);
		}

		let container = document.querySelector('script') || document.body;
		
		script = document.createElement('script');
		script.id = "gapi";
		script.src = "https://apis.google.com/js/platform.js?onload=onGAPILoaded";
		
		container.parentNode.insertBefore(script, container);
	}

	_onGAPILoaded() {
		delete window[this.initCallback];
		this.onGAPILoaded();
	}

	onGAPILoaded() {
		const that = this;
		gapi.load("auth2", function() {
			gapi.auth2.init({
				client_id: '1055723309200-pi4ag3opg7q97a4gmcleun5cpfc697qc.apps.googleusercontent.com',
				scope: 'email profile openid',
				fetch_basic_profile: true
			}).then(that.onGoogleAuthInit.bind(that));
		});
	}

	onGoogleAuthInit(gauth) {
		this.gauth = gauth;
		this.loginReady = true;
	}

	doLogin() {
		this.loginReady = false;
		this.gauth.signIn().then(this.onLoginSuccess.bind(this));
	}

	onLoginSuccess(user) {
		const profile = user.getBasicProfile();
		const authz = user.getAuthResponse(true);

		const _user = {
			id: profile.getId(),
			email: profile.getEmail(),
			name: profile.getName(),
			image: profile.getImageUrl(),
			token: authz['id_token']
		};

		this.email = _user.email;
		this.name = _user.name;

		store.dispatch(setGAuth(_user));
		store.dispatch(login(_user));
	}

	stateChanged(state) {

		const { auth } = state;

		if( !auth.id ) { return; }

		this.user = auth.id;
		setTimeout(function(url) {
			Router.go(url);
		}, 1000, "/")
	}

}

customElements.define('google-login', GoogleLogin);
