import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';

import Chart from 'chart.js';
import * as R from 'ramda'

const {  mapObjIndexed, pick, values, forEach } = R

class AccountBalanceChart extends connect(store)(LitElement) {

	static get properties() {
		return {
			account: { type: Object },
			chartData: { type: Object },
			chartOptions: { type: Object },
			myChart: { type: Object },
			datasets: { type: Object },
			chartConfig: { type: Object }
		};
	}

	constructor() {
		super();

		this.chartConfig = {
			type: 'line',
			data: {
				labels: [],
				datasets: []
			},
			options: {
				responsive: true,
				title: {
					display: false
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'FECHA'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'BALANCE'
						}
					}]
				}
			}
		};

	}

	firstUpdated(changedProperties) { 
		const ctx = this.renderRoot.querySelector('#chart').getContext('2d');
		this.myChart = new Chart(ctx, this.chartConfig);
	}

	render() {
		return html`
			<div style="position: relative; height: 15vh; width: 75vw">
				<canvas id="chart"></canvas>
			</div>
		`;
	}

	updated(changedProperties) {

		if( !changedProperties.has('datasets') ) { return; }

		console.log(this.datasets);

		const that = this;

		const datasets = values(mapObjIndexed(function(value, key) {
			const color = that.getColor(key);
			const label = that.getLabel(key);
			return { 
				data: value,
				fill: false,
				'label': label,
				backgroundColor: color,
				borderColor: color,
			};
		}, pick(['totals', 'negatives', 'positives'], this.datasets)));

		this.chartConfig.data.labels = this.datasets.labels;
		this.chartConfig.data.datasets = datasets;

		this.myChart.update();
	}

	getLabel(label) {
		switch(label) {
			case 'totals':
				return "SALDO";
			case 'negatives':
				return "CARGOS";
			case 'positives':
				return "ABONOS";
			default: /* NO_GO */
		}
		return "???";
	}

	getColor(label) {
		switch(label) {
			case 'totals':
				return "#0036a3";
			case 'negatives':
				return "#ef0036";
			case 'positives':
				return "#00d600";
			default: /* NO_GO */
		}
		return "#ffffff";
	}

	stateChanged(state){
	}

}

customElements.define('account-balance-chart', AccountBalanceChart);
