import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { Router } from '@vaadin/router';
import { store } from '../store.js';
import { getAccounts } from '../actions';
import * as R from 'ramda'

const { equals } = R

class MainPage extends connect(store)(LitElement) {

	static get properties() {
		return {
			accounts: { type: Array }
		};
	}

	onBeforeEnter(location, commands) {
		store.dispatch(getAccounts());
	}

	constructor() {
		super();
		this.accounts = []
	}

	updated(changedProperties) { 
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		const { accounts } = this;
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div class="card drop-shadow w-75">
				<div class="card-header fg-darkCobalt text-bold">CUENTAS</div>
				<div class="card-content p-2">
					<ul class="items-list">
					${accounts.map(account => html`
						<li>
							<span class="label"><b>${account.id}</b></span>
							<span class="second-label">${account.desc}</span>
							<span class="second-action">
								<button class="button small primary cycle drop-shadow" @click="${(e) => this.selectAccount(account)}">
									<span class="mif-chevron-thin-right"></span>
								</button>
							</span>
						</li>
					`)}
					</ul>
				</div>
				<div class="card-footer">
					<button class="button secondary text-bold" @click="${this.updateAccounts}">Actualizar</button>
				</div>
			</div>
			${this.script()}
		`;
	}

	updateAccounts() {
		store.dispatch(getAccounts());
	}

	selectAccount(account) {
		Router.go(`/accounts/${account.id}`);
	}

	stateChanged(state){
		const { ui, auth, accounts } = state;

		const _accounts = (accounts.allAccounts || []);

		if( !equals(this.accounts, _accounts) ) {
			this.accounts = _accounts
		}
	}

}

customElements.define('main-page', MainPage);
