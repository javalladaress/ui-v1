import { LitElement, html } from '@polymer/lit-element/';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import * as R from 'ramda'

const { merge } = R

class EditAccountDialog extends connect(store)(LitElement) {

	static get properties() {
		return {
			account: { type: Object },
			opened: { type: Boolean },
			updating: { type: Boolean },
			description: { type: String }
		};
	}

	constructor() {
		super();
		this.opened = false;
		this.updating = false;
		this.description = "";
	}

	firstUpdated(changedProperties) { 
		Metro.initWidgets([
			this.shadowRoot.getElementById("editAccountDialog"),
			this.shadowRoot.getElementById("accountDescription")
		]);
	}

	disconnectedCallback() {
		const dialog = document.getElementById("editAccountDialog");
		dialog.parentNode.removeChild(dialog);
	}

	script() {
		let script = document.createElement('script');
		script.src = 'https://cdn.metroui.org.ua/v4/js/metro.min.js';
		return script;
	}

	render() {
		const { id, desc } = this.account;
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4.3.2/css/metro-all.min.css">
			<div id="editAccountDialog" class="dialog" data-role="dialog">
				<div class="dialog-title fg-cyan text-bold"><b>${id}</b></div>
				<div class="dialog-content">
					<textarea id="accountDescription" data-role="textarea"  data-clear-button="false" .value="${this.description}"
								@input="${(e) => this.updateDescription(e)}" ?disabled="${this.updating}">${desc}</textarea>
				</div>
				<div class="dialog-actions">
					<button class="button secondary" @click="${this.cancelDialog}" ?disabled="${this.updating}">Cancelar</button>
					<button class="button primary" @click="${this.updateAccount}" ?disabled="${this.updating}">Actualizar</button>
				</div>
			</div>
			${this.script()}
		`;
	}

	updateAccount() {
		this.updating = true;
		this.dispatchEvent(new CustomEvent('dialog-confirm', { 
			detail: merge(this.account, { desc: this.description })
		}));
	}

	updateDescription(e) {
		this.description = e.target.value;
	}

	cancelDialog() {
		this.description = this.account.desc;
		this.dispatchEvent(new CustomEvent('dialog-cancel'));
	}

	updated(changedProperties) {

		if( changedProperties.has('opened') ) {

			if( this.opened ) {
				Metro.dialog.open('#editAccountDialog');
			} else {
				Metro.dialog.close('#editAccountDialog');
				this.updating = false;
			}

		}

		if( changedProperties.has('account') ) {
			this.description = this.account.desc || "";
		}
	}

	stateChanged(state){
	}

}

customElements.define('edit-account-dialog', EditAccountDialog);
