import { LitElement, html } from '@polymer/lit-element';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { Router } from '@vaadin/router';
import { store } from './store.js';

import * as R from 'ramda'
const { isEmpty } = R

import { setRouter } from './actions';

import './components/main-page.js';
import './components/not-found.js';
import './components/google-login.js';
import './components/account-details.js';
import './components/app-logout.js';

class LitApp extends connect(store)(LitElement) {

	static get properties() {
		return {
			loggedin: { type: Boolean },
			terminating: { type: Boolean },
			name: { type: String },
			email: { type: String },
			image: { type: String }
		};
	}

	constructor() {
		super();
		this.loggedin = false;
		this.terminating = false;
	}

	firstUpdated(){
		const router = new Router(this.shadowRoot.querySelector('#outlet'));

		router.setRoutes([
			{ path: '/login', component: 'google-login' },
			{ path: '/logout', component: 'app-logout' },
			{ path: '/', component: 'main-page' },
			{ path: '/accounts/:id', component: 'account-details' },
			{ path: '(.*)', component: 'not-found' }
		]);

		store.dispatch(setRouter(router));
	}

	render() {
		return html`
			<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4/css/metro-all.min.css">
			<div class="app">
				<div class="bg-darkCobalt fg-white py-4 px-4 drop-shadow" ?hidden="${this.terminating}">
					<img class="drop-shadow mr-4" src="${this.image}" style="width:32px;height:auto;border-radius:50%;overflow:hidden;" ?hidden="${(!this.loggedin || this.terminating)}">
					<span class="app-title h4 mb-2 text-bold">BBVA | Cuentas</span>
					<span class="place-right text-small text-bold mt-2 mr-16" ?hidden="${(!this.loggedin || this.terminating)}">${this.email}</span>
					<button @click="${this.doLogout}" class="image-button primary large p-2 mt-4 mr-2 pos-fixed pos-top-right drop-shadow text-bold" id="logout" ?hidden="${(!this.loggedin || this.terminating)}">Logout</button>
				</div>
				<div id="outlet" class="container-fluid w-75 mt-4"></div>
			</div>
		`;
	}

	doLogout() {
		const { auth } = store.getState();
		if( !auth || isEmpty(auth) ) { return }
		this.terminating = true;
		this.loggedin = false;
		Router.go("/logout");
	}

	stateChanged(state) {
		const { ui, auth, gauth } = state;

		if( !ui.router ) { return; }

		const location = ui.router.location;

		if( (!auth || isEmpty(auth) || !auth.id)
				&& (location.getUrl() != "/logout") ) {
			console.log(location.getUrl());
			this.loggedin = false;
			this.terminating = true;
			Router.go("/login")
			return;
		}

		if( !gauth || isEmpty(gauth) ) { return; }

		this.terminating = false;

		this.email = gauth.email;
		this.image = gauth.image;

		this.loggedin = true;
	}
}

customElements.define('lit-app', LitApp);
