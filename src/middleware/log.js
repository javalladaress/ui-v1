const log = ({ getState, dispatch }) => next => action => {
    next(action);
    console.log("ACTION: ", action.type, action);
};

export default log;