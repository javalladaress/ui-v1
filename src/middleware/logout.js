import * as actions from '../consts/action-types';
import { 
    unsetAccount,
    unsetAccounts
} from '../actions';

const logout = ({ getState, dispatch }) => next => action => {

    if( action.type !== actions.LOGOUT ) {
        return next(action); 
    }

    localStorage.removeItem('APP_STATE');

    dispatch({ type: actions.UNSET_GAUTH });
    dispatch({ type: actions.UNSET_AUTH });
    
    dispatch(unsetAccount());
    dispatch(unsetAccounts());

};

export default logout;