import * as actions from '../consts/action-types';
import { logout } from '../actions';
const axios = require('axios').default;

axios.interceptors.response.use(response => response, 
    error => {
        if (error.response.status === 401) {
            
        }
        return error;
    }
);

const client = axios.create({  
    baseURL: "https://34.123.103.105.xip.io:8443"
});

const api = ({ getState, dispatch }) => next => action => {
    
    if( action.type !== actions.API ) {
        return next(action);
    }

    const { auth } = getState();
    
    const { method, url, onSuccess } = action.meta;
    const headers = {};

    if( auth ) {
        headers['Authorization'] = ["Bearer", auth.token].join(" ");
    }

    headers['Accept'] = "application/json";

    client({ method, url, headers, data: action.payload })
    .then(response => dispatch(onSuccess(response.data)))
    .catch(function (error) {
        if (error.response.status === 401) {
            dispatch(logout(auth))
        }
    });

};

export default api;