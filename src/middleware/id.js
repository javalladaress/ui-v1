import { nanoid } from 'nanoid';

const id = ({ getState, dispatch }) => next => action => {
    action.id = nanoid();
    next(action);
};

export default id;